import { Provider } from '@nestjs/common';
import { MessageBrokerModuleAsyncOptions } from './interfaces';
import { MESSAGE_BROKER_MODULE_OPTIONS } from './message-broker.constants';

export const createAsyncOptionsProvider = (
  options: MessageBrokerModuleAsyncOptions,
): Provider => ({
  provide: MESSAGE_BROKER_MODULE_OPTIONS,
  useFactory: options.useFactory,
  inject: options.inject,
});
