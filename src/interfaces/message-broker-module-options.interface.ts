import { ModuleMetadata } from '@nestjs/common';

export type MessageBrokerHandlers = Record<string, MessageHandlerOptions>;

export interface MessageHandlerOptions {
  routingKey?: string | string[];
  queue?: string;
}

export interface MessageBrokerModuleOptions {
  handlers?: MessageBrokerHandlers;
  uri: string;
  exchange: MessageBrokerExchange;
  name: string;
}

export type MessageBrokerExchange = {
  name: string;
  durable: boolean;
};

export interface MessageBrokerModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  useFactory?: (
    ...args: any[]
  ) => MessageBrokerModuleOptions | Promise<MessageBrokerModuleOptions>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  inject?: any[];
}
