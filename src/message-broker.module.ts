import {
  RabbitMQConfig,
  RabbitMQHandlers,
  RabbitMQModule,
} from '@golevelup/nestjs-rabbitmq';
import { DynamicModule, Module, Provider } from '@nestjs/common';
import { createAsyncOptionsProvider } from './message-broker.provider';
import {
  MessageBrokerHandlers,
  MessageBrokerModuleAsyncOptions,
  MessageBrokerModuleOptions,
} from './interfaces';
import {
  MESSAGE_BROKER_EXCHANGE,
  MESSAGE_BROKER_MODULE_OPTIONS,
} from './message-broker.constants';
import { MessageBrokerPublisher } from './message-broker.publisher';

@Module({})
export class MessageBrokerModule {
  static forRootAsync(options: MessageBrokerModuleAsyncOptions): DynamicModule {
    const asyncOptionsProvider: Provider = createAsyncOptionsProvider(options);

    const imports = [
      RabbitMQModule.forRootAsync(RabbitMQModule, {
        inject: [MESSAGE_BROKER_MODULE_OPTIONS],
        useFactory: async (
          options: MessageBrokerModuleOptions,
        ): Promise<RabbitMQConfig> => ({
          exchanges: [
            {
              options: {
                durable: options.exchange.durable,
              },
              name: options.exchange.name,
              type: 'topic',
            },
          ],
          handlers: createRabbitMQHandlers(
            options.exchange.name,
            options.handlers,
          ),
          uri: options.uri,
          connectionInitOptions: { wait: false },
          enableControllerDiscovery: false,
          name: options.name,
        }),
      }),
    ];

    return {
      global: true,
      module: MessageBrokerModule,
      imports,
      providers: [
        asyncOptionsProvider,
        {
          provide: MESSAGE_BROKER_EXCHANGE,
          useFactory: async (
            options: MessageBrokerModuleOptions,
          ): Promise<string> => options.exchange.name,
          inject: [MESSAGE_BROKER_MODULE_OPTIONS],
        },
        MessageBrokerPublisher,
      ],
      exports: [
        asyncOptionsProvider,
        MessageBrokerPublisher,
        RabbitMQModule,
        MESSAGE_BROKER_EXCHANGE,
      ],
    };
  }
}

const createRabbitMQHandlers = (
  exchangeName: string,
  handlers: MessageBrokerHandlers,
): RabbitMQHandlers => {
  const rabbitMqHandlers = {};
  if (handlers) {
    const handlersKeys = Object.keys(handlers);
    const handlersArray = Object.values(handlers);
    handlersKeys.forEach((key, index) => {
      rabbitMqHandlers[key] = {
        ...handlersArray[index],
        exchange: exchangeName,
      };
    });
  }
  return rabbitMqHandlers;
};
