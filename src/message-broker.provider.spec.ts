import { Provider } from '@nestjs/common';
import { MessageBrokerModuleAsyncOptions } from './interfaces';
import { createAsyncOptionsProvider } from './message-broker.provider';

describe('Message broker provider', () => {
  it('should return an async options provider', async () => {
    const messageBrokerModuleAsyncOptions: MessageBrokerModuleAsyncOptions = {
      useFactory: jest.fn(),
      imports: [jest.fn()],
      inject: [jest.fn()],
    };
    const expectedProvider: Provider = createAsyncOptionsProvider(
      messageBrokerModuleAsyncOptions,
    );
    expect(expectedProvider).toBeDefined();
  });
});
