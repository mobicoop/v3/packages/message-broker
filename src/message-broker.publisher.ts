import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Inject, Injectable } from '@nestjs/common';
import { MESSAGE_BROKER_EXCHANGE } from './message-broker.constants';

@Injectable()
export class MessageBrokerPublisher {
  constructor(
    private readonly amqpConnection: AmqpConnection,
    @Inject(MESSAGE_BROKER_EXCHANGE)
    protected readonly exchange: string,
  ) {}

  publish = (routingKey: string, message: string): void => {
    this.amqpConnection.publish(this.exchange, routingKey, message);
  };
}
