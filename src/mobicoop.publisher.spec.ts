import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Test, TestingModule } from '@nestjs/testing';
import { MessageBrokerPublisher } from './message-broker.publisher';
import { MESSAGE_BROKER_EXCHANGE } from './message-broker.constants';

const mockAmqpConnection = {
  publish: jest.fn().mockImplementation(),
};

describe('Message broker publisher', () => {
  let publisher: MessageBrokerPublisher;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        MessageBrokerPublisher,
        {
          provide: AmqpConnection,
          useValue: mockAmqpConnection,
        },
        {
          provide: MESSAGE_BROKER_EXCHANGE,
          useValue: 'mobicoop',
        },
      ],
    }).compile();

    publisher = module.get<MessageBrokerPublisher>(MessageBrokerPublisher);
  });

  it('should be defined', () => {
    expect(publisher).toBeDefined();
  });

  it('should publish a message', () => {
    jest.spyOn(mockAmqpConnection, 'publish');
    publisher.publish('test.create.info', 'my-test');
    expect(mockAmqpConnection.publish).toHaveBeenCalledTimes(1);
  });
});
