## Demande de fusion de nouvelle fonctionnalité

### Anomalies/Evolutions Redmine

_Selectionner "Close" seulement si cette MR est la dernière liée au ticket, sinon selectionner "Part of"_

Close / Part of V3-XXXX

### Pourquoi cette demande de fusion ?

_Indiquer qu'est ce que cette demande fusion traite, et décrire le problème ou le récit utilisateur qui est traité._

### Qu'est ce qui est implémenter, quelle est la solution choisie ?

_Expliquer la correction ou la solution mise en oeuvre. Quelles autres solutions ont été envisagées._

### Anomalies/évolutions lées et impact sur les autres projets dans le codebase

_Fournir les liens vers les anomalies/évolutions liées, ticket d'anomalie/fusion et demande de fusion (sur Gitlab et Redmine)._


_Et lien vers les autres projets impactés._

#### A fusionner avant cette MR

- [ ] _lien vers MR ici_
- [ ] ...

### A fusionner après cette MR

- [ ] _lien vers MR ici_
- [ ] ...

### Autres informations

_Inclure toute information ou remarque supplémentaire pour les relecteurs._


## Liste de vérifications

### Demande de fusion

- [ ] La branche cible identifiée.
- [ ] La description et les dépendances de la demande de fusion sont complétées.
- [ ] La documentation reflète les changements éffectués.
- [ ] Un ou plusieurs relecteurs ont été assignés.

### Revue de code

Ici quelques idées de choses que vous pourriez vouloir vérifier lors de la révision de la MR :

- [ ] Le code suit l'architecture du projet.
- [ ] Tout ce qui est nouveau a un nom explicite, pertinent et cohérent.
- [ ] Pas de code duppliqué/redondant (sauf si expliqué par l'architecture).
- [ ] Les commits sont atomics et utilisent des messages conventionnels.
- [ ] L'ensemble des commits ne concernent qu'un seul sujet (pas de mise à jour de dépendances sauf si nécessaire par la MR, pas de changement non lié)
- [ ] Le nouveau code est testé et couvert par des tests automatisés.
- [ ] Absence de code inutile ou de code de débogage.
- [ ] Le code n'est redondant (DRY) et ne fait pas double emploi avec d'autres parties du code.
