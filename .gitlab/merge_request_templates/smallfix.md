_Replace italic text by your own description_

## Small Fix Merge Request

### Why this Merge Request

_This merge request addresses, and describe the problem or user story being addressed._

### What is implemented, what is the chosen solution

_Explain the fix or solution implemented. Which other solution have been envisaged._

### Related issues and impact on other project in codebase

_Provide links to the related issues, feature requests and merge request (from Gitlab and Redmine)._

_And Link to other project Impacted._

### Other Information

_Include any extra information or considerations for reviewers._

## Checklists

### Merge Request

- [ ] Target branch identified.
- [ ] Code based on last version of target branch.
- [ ] Description filled.
- [ ] Impact on other project codebase identified.
- [ ] Test run in gitlab pipeline and locally.

### Code Review

- [ ] Code is easily readable.
- [ ] Commit are all related to MR and well written (Atomic commit).
- [ ] No useless logging or debugging code.
