# Mobicoop V3 - Message broker package

Message broker NPM package for Mobicoop V3 services. Encapsulates [@golevelup/nestjs-rabbitmq](https://github.com/golevelup/nestjs/tree/master/packages/rabbitmq), and simplifies its usage by restricting configuration to the minimum required for Mobicoop V3 (eg. only one exchange).

## Requirements

-   a running RabbitMQ server

## Installation

```bash
npm install --save @mobicoop/message-broker-module
```

## Usage

Add the module in the _imports_ section of the parent module, for example, using NestJs ConfigModule to inject values from `.env`:

```ts
...
import {
  MessageBrokerModule,
  MessageBrokerModuleOptions,
} from '@mobicoop/message-broker-module';
import { ConfigModule, ConfigService } from '@nestjs/config';
...
imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MessageBrokerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (
        configService: ConfigService,
      ): Promise<MessageBrokerModuleOptions> => ({
        uri: configService.get<string>('MESSAGE_BROKER_URI'),
        exchange: {
            name: configService.get<string>('MESSAGE_BROKER_EXCHANGE_NAME'),
            durable: configService.get<boolean>('MESSAGE_BROKER_EXCHANGE_DURABILITY'),
        }
      }),
    }),
],
  ...
```

You need to set the following options :

-   `uri` : the uri of the message broker
-   `exchange` : the name of the exchange, and the durability
-   `name` (optional) : the name of the instance (useful for logs)

## Message subscribing

To subscribe to messages, you must set `handlers` in the factory function :

```ts
...
import {
  MessageBrokerModule,
  MessageBrokerModuleOptions,
} from '@mobicoop/message-broker-module';
import { ConfigModule, ConfigService } from '@nestjs/config';
...
imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MessageBrokerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (
        configService: ConfigService,
      ): Promise<MessageBrokerModuleOptions> => ({
        uri: configService.get<string>('MESSAGE_BROKER_URI'),
        exchange: {
            name: configService.get<string>('MESSAGE_BROKER_EXCHANGE_NAME'),
            durable: configService.get<boolean>('MESSAGE_BROKER_EXCHANGE_DURABILITY'),
        }
        handlers: {
          adCreated: {
            routingKey: 'ad.created',
            queue: 'message-broker-ad-created',
          },
        },
      }),
    }),
],
...
```

`handlers` is an object containing the handlers that will subscribe to the `routingKey`, on the given `queue`, under the form :

```ts
handlers: {
    someHandlerName: {
        routingKey: 'some.routing.key.',
        queue: 'some-queue',
    },
    anotherHandlerName: {
        routingKey: 'another.routing.key.',
        queue: 'another-queue',
    },
    ...
}
```

Then you need to use the `@RabbitSubscribe` annotation, using a routingKey defined in the handlers, eg. :

```ts
...
@RabbitSubscribe({
    name: 'someHandlerName',
})
async myHandlerFunction(message: string): {
    ...
}
...
```

Note that the module is available globally, you must define it only once in your project and declare all required handlers in the same place.

## Message publishing

To publish messages to the broker, you must inject `MessageBrokerPublisher` in your service :

```ts
...
constructor(
    private readonly messageBrokerPublisher: MessageBrokerPublisher,
) {}
...
```

Then you can publish a message :

```ts
...
this.messageBrokerPublisher.publish(
    'my.routing.key',
    JSON.stringify({
        message: 'A nice message !',
    }),
);
...
```

_Note that all messages are received / sent via the previously set `exchange`_
